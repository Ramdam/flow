import React from 'react' ;
import { Dimensions, StyleSheet, View } from 'react-native' ;
import { Overlay } from 'react-native-elements' ;

const MyModal = ( { visible, hide, children } ) => {

    return (
        <Overlay
            animated
            animationType = "slide"
            visible = { visible }
            onBackdropPress = { () => hide() }
            overlayStyle = {styles.overlay}
        >
            <View style = { styles.container }>
                {children}
            </View>
        </Overlay>
    )

}


const styles = StyleSheet.create({

    overlay: {
        width : '100%',
        height : 250,
        top : Dimensions.get('window').height-480,
        backgroundColor : 'transparent',
        flexDirection : 'column',
        justifyContent : 'flex-end',
        margin : 0,
        padding : 0,
        borderWidth : 0,
    },

    container: {
        width : '100%',
        backgroundColor : '#ffffff',
        paddingTop : 12,
        borderTopRightRadius : 12,
        borderTopLeftRadius : 12,
        flexDirection : 'column',
        justifyContent : 'center',
        alignItems : 'center',
    },

})

export default MyModal ;