import React, { useState, useEffect } from 'react' ;
import { StyleSheet } from 'react-native' ;
import { Surface } from 'react-native-paper' ;

import MoodboardButton from '../Components/moodboardbutton.js' ;

import moodEmoji00 from '../Assets/sprites/moodemoji_00.png' ;
import moodEmoji01 from '../Assets/sprites/moodemoji_01.png' ;
import moodEmoji02 from '../Assets/sprites/moodemoji_02.png' ;
import moodEmoji03 from '../Assets/sprites/moodemoji_03.png' ;
import moodEmoji04 from '../Assets/sprites/moodemoji_04.png' ;

const Moodboard = ( { initialState, onSelectionChange }) => {

    const [ index, setIndex ] = useState(initialState === undefined ? 2 : initialState ) ;

    useEffect( () => {
        onSelectionChange(index) ;
    }, [index])

    return (
        <Surface style = { styles.headerMoodboardContainer }>
            <MoodboardButton onPress = { () => setIndex(4) } selected = { index === 4 } source = { moodEmoji04 }/>
            <MoodboardButton onPress = { () => setIndex(3) } selected = { index === 3 }  source = { moodEmoji03 } />
            <MoodboardButton onPress = { () => setIndex(2) } selected = { index === 2 }  source = { moodEmoji02 } />
            <MoodboardButton onPress = { () => setIndex(1) } selected = { index === 1 }  source = { moodEmoji01 } />
            <MoodboardButton onPress = { () => setIndex(0) } selected = { index === 0 }  source = { moodEmoji00 } />
        </Surface>
    ) ;
}

const styles = StyleSheet.create({

    headerMoodboardContainer : {      
        marginLeft : '10%',
        top : '5%',
        width : '80%',
        height : '40%',
        backgroundColor : 'transparent',
        flexDirection : 'row',
        justifyContent : 'space-evenly',
        alignItems : 'center',
    },
    
})

export default Moodboard ;