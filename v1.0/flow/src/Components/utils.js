export const PopulateStartIntervals = () => {

    let array = []

    let items = [0, 1, 2] ;
    items.map( item => 
        array.push({
            'time' : Math.round(item * (60/items.length)),
            'stars' : item + 1,
        })
    )

    items = [0, 1, 2, 3, 4] ;
    items.map( item => 
        array.push({
            'time' : 60 + Math.round(item * (60/items.length)),
            'stars' : item + 1 + 3,
        })
    )

    items = [0, 1, 2, 3, 4, 5, 6, 7] ;
    items.map( item => 
        array.push({
            'time' : 120 + Math.round(item * (60/items.length)),
            'stars' : item + 1 + 3 + 5,
        })
    )

    items = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12] ;
    items.map( item => 
        array.push({
            'time' : 180 + Math.round(item * (60/items.length)),
            'stars' : item + 1 + 3 + 5 + 8,
        })
    )

    items = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20] ;
    items.map( item => 
        array.push({
            'time' : 240 + Math.round(item * (60/items.length)),
            'stars' : item + 1 + 3 + 5 + 8 + 13,
        })
    )
    
    return array ;
}