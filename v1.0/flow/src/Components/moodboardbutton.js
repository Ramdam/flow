import React from 'react' ;
import { Surface, IconButton } from 'react-native-paper' ;

const MoodboardButton = ({ source, selected, onPress }) => {

    return (
        <Surface style = { selected ? { elevation : 0, borderRadius : 24, padding : 0, margin : 0, } : { elevation : 5, borderRadius : 21, padding : 0, margin : 0, } }>
            <IconButton
                icon = { source }
                color = { selected ? '#ffffff' : 'black' }
                size = { selected ? 24 : 21 }
                compact = { true }
                onPress = { onPress }
                animated = { true }
                style = {
                    selected ?
                    {
                        margin : 0,
                        padding : 5,
                        backgroundColor : '#7DBBB9'
                    } :
                    {
                        margin : 2,
                        padding : 5,
                        backgroundColor : 'rgba(125, 187, 185, 0.2)',
                    }
                }
            />
        </Surface>
    )
}

export default MoodboardButton ;