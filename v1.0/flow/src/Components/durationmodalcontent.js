import React, { useState } from 'react' ;
import { StyleSheet } from 'react-native' ;
import { Surface, Text, IconButton, Button } from 'react-native-paper' ;

import IncreaseIcon from '../Assets/sprites/increase_icon.png' ;
import DecreaseIcon from '../Assets/sprites/decrease_icon.png' ;

const DurationModalContent = ( { onValidate } ) => {

    const [ hours, setHours ] = useState(0) ;
    const [ minutes, setMinutes ] = useState(5) ;

    const increaseHours = () => {
        if (hours < 24 )
            setHours(oldValue => oldValue + 1) ;
    }
    const decreaseHours = () => {
        if (hours > 0 )
            setHours(oldValue => oldValue - 1) ;
    }

    const increaseMinutes = () => {
        if (minutes < 60 )
            setMinutes(oldValue => oldValue + 1) ;
    }
    const decreaseMinutes = () => {
        if (minutes > 0 )
            setMinutes(oldValue => oldValue -1) ;
    }

    return (
        <Surface style = { styles.durationModalContentContainer }>
            <Text style = { styles.durationModalContentTitle }>Durée</Text>
            <Surface style = { styles.durationModalContentOptions}>

                <Surface style = { styles.durationTimeSelectionContainer}>
                    <IconButton
                        icon = { IncreaseIcon }
                        color = { '#7dbbb9' }
                        size = { 32 }
                        compact = { true }
                        onPress = { () => increaseHours() }
                        animated = { true }
                        style = {{ margin : 0, padding : 10, backgroundColor : '#ffffff' }}
                    />
                    <Surface style = { styles.durationTimeSelectionLabel }>
                        <Text style = { styles.durationTimeSelectionLabelContent }>{hours}</Text>
                    </Surface>
                    <IconButton
                        icon = { DecreaseIcon }
                        color = { '#7dbbb9' }
                        size = { 32 }
                        compact = { true }
                        onPress = { () => decreaseHours() }
                        animated = { true }
                        style = {{ margin : 0, padding : 10, backgroundColor : '#ffffff' }}
                    />
                </Surface>
                
                <Text style = { styles.durationTimeSelectionText }>{ hours > 1 ? 'Heures' : 'Heure'}</Text>

                <Surface style = { styles.durationTimeSelectionContainer}>
                    <IconButton
                        icon = { IncreaseIcon }
                        color = { '#7dbbb9' }
                        size = { 32 }
                        compact = { true }
                        onPress = { () => increaseMinutes() }
                        animated = { true }
                        style = {{ margin : 0, padding : 10, backgroundColor : '#ffffff' }}
                    />
                    <Surface style = { styles.durationTimeSelectionLabel }>
                        <Text style = { styles.durationTimeSelectionLabelContent }>{minutes}</Text>
                    </Surface>
                    <IconButton
                        icon = { DecreaseIcon }
                        color = { '#7dbbb9' }
                        size = { 32 }
                        compact = { true }
                        onPress = { () => decreaseMinutes() }
                        animated = { true }
                        style = {{ margin : 0, padding : 10, backgroundColor : '#ffffff' }}
                    />
                </Surface>
                <Text style = { styles.durationTimeSelectionText }>{minutes > 1 ? 'Minutes' : 'Minute'}</Text>

            </Surface>
            <Button
                mode = 'contained'
                color = "#7dbbb9"
                contentStyle = { styles.modalButtonContent }
                style = { styles.modalButton }
                labelStyle = { styles.modalButtonLabel } 
                onPress = { () => onValidate(hours * 60 + minutes) }
            >
                Enregistrer
            </Button>
        </Surface>

    )

}

const styles = StyleSheet.create({

    durationModalContentContainer : {
        width : '100%',
        height : 250,
        flexDirection : 'column',
        justifyContent : 'center',
        alignItems : 'center',
    },
    
    durationModalContentTitle : {
        fontFamily : 'Raleway',
        fontWeight : 'bold',
        fontSize : 18,
        lineHeight : 21, 
        color: '#000000',
        height : '10%',
    },
    
    durationModalContentOptions : {
        flexDirection : 'row',
        width : '80%',
        height : '70%',
        justifyContent : 'center',
        alignItems : 'center',
        marginLeft : 40,
    },
    
    durationTimeSelectionContainer : {
        flexDirection : 'column',
        height : '80%',
        width : '20%',
        justifyContent : 'center',
        alignItems : 'center',
    },

    durationTimeSelectionLabel : {
        backgroundColor : '#f3f3f3',
        borderRadius : 10,
        width : 40,
        height : 60,
        justifyContent : 'center',
        alignItems : 'center',
    },
    
    durationTimeSelectionLabelContent : {
        fontFamily : 'Raleway',
        fontSize : 24,
        lineHeight : 28,
    },
    
    durationTimeSelectionText : {
        marginRight : 40,
        fontFamily : 'Raleway',
        fontSize : 12,
        lineHeight : 14, 
        color: '#000000',
    },

    modalButtonContainer : {

        width : '35%',
        height : '35%',
        margin : 0,
        padding : 0,

    },

    modalButtonContent : {

        margin : 0,
        padding : 0,

    },

    modalButton : {

        margin : 0,
        padding : 0,
        borderRadius : 20,
        borderWidth : .5,
        borderColor : '#7dbbb9',

    },

    modalButtonLabel : {

        color : "#ffffff",
        fontSize : 10,
        lineHeight : 10,

    },

})

export default DurationModalContent ;