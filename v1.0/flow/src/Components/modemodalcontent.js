import React, { useState } from 'react' ;
import { StyleSheet } from 'react-native' ;
import { Surface, Text, Button, Checkbox } from 'react-native-paper' ;

const ModeModalContent = ( { options, onValidate, currentIndex } ) => {

    const [ currentSelection, setCurrentSelection ] = useState(currentIndex) ;

    return (
        <Surface style = { styles.modeModalContentContainer }>
            <Text style = { styles.modeModalContentTitle }>Mode</Text>
            <Surface style = { styles.modeModalContentOptions}>
                {
                    options.map ( (item, index) => {
                        return (
                            <Surface key = {'option_' + index} style = { styles.modeModalContentOption }>
                                <Text style = { styles.modeModalContentOptionTitle }>{item.title}</Text>
                                <Text style = { styles.modeModalContentOptionDescription }>{item.description}</Text>
                                <Checkbox
                                    status = { currentSelection === index ? 'checked' : 'unchecked' }
                                    onPress={() => {
                                        setCurrentSelection(index) ;
                                    }}
                                    uncheckedColor = '#aeaeae'
                                    color = '#7DBBB9'
                                />
                            </Surface>
                        )
                    })
                }
            </Surface>
            <Button
                mode = 'contained'
                color = "#7dbbb9"
                contentStyle = { styles.modalButtonContent }
                style = { styles.modalButton }
                labelStyle = { styles.modalButtonLabel } 
                onPress = { () => onValidate(currentSelection) }
            >
                Enregistrer
            </Button>
        </Surface>
    )

}



const styles = StyleSheet.create({

    modeModalContentContainer : {
        width : '100%',
        height : 250,
        flexDirection : 'column',
        justifyContent : 'center',
        alignItems : 'center',
    },

    modeModalContentTitle : {
        fontFamily : 'Raleway',
        fontWeight : 'bold',
        fontSize : 18,
        lineHeight : 21, 
        color: '#000000',
        height : '10%',
    },

    modeModalContentOptions : {
        flexDirection : 'column',
        justifyContent : 'space-between',
        height : '70%',
        width : '90%',
    },

    modeModalContentOption : {
        flexDirection : 'column',
        justifyContent : 'center',
        alignItems : 'center',
        flexWrap : 'wrap',
        height : '33%',
    },

    modeModalContentOptionTitle : {
        fontFamily : 'Raleway',
        fontWeight : 'bold',
        fontSize : 18,
        lineHeight : 21,
        color: '#000000',
        textAlign : 'left',
        width : '80%',
        height : '30%',
        marginTop : 10,
        paddingLeft : 10,
    },

    modeModalContentOptionDescription : {
        fontFamily : 'Raleway',
        fontSize : 10,
        lineHeight : 12,
        color: '#000000',
        textAlign : 'left',
        width : '80%',
        height : '50%',
        paddingLeft : 10,
    },


    modalButtonContainer : {
        width : '35%',
        height : '35%',
        margin : 0,
        padding : 0,
    },

    modalButtonContent : {
        margin : 0,
        padding : 0,
    },

    modalButton : {
        margin : 0,
        padding : 0,
        borderRadius : 20,
        borderWidth : .5,
        borderColor : '#7dbbb9',
    },

    modalButtonLabel : {
        color : "#ffffff",
        fontSize : 10,
        lineHeight : 10,
    },

})

export default ModeModalContent ;