import React from 'react' ;
import { View, Image, Dimensions } from 'react-native' ;

import cloudSprite from '../Assets/sprites/cloud.png' ;

const Cloud = ( { scale, top, left }) => {

    const scaledWidth = Math.round(54 * scale) ;
    const scaledHeight = Math.round(36 * scale) ;

    return(
        <Image source = { cloudSprite } style = { { position : 'absolute', top : top, left : left, width : scaledWidth, height : scaledHeight }} />
    )
}


const Clouds = () => {

    const windowWidth = Dimensions.get('window').width ;
    const windowHeight = Dimensions.get('window').height ;

    return (
        <View>
            <Cloud scale = { 1.3 } top = { windowHeight * .1 } left = { windowWidth * .1 } />
            <Cloud scale = { 1.0 } top = { windowHeight * .2 } left = { windowWidth * .15 } />
            <Cloud scale = { 1.4 } top = { windowHeight * .4 } left = { windowWidth * .2 } />
            <Cloud scale = { 0.6 } top = { windowHeight * .6 } left = { windowWidth * .05 } />
            <Cloud scale = { 1.8 } top = { windowHeight * .65 } left = { windowWidth * .0 } />
            <Cloud scale = { 1.4 } top = { windowHeight * .85 } left = { windowWidth * .1 } />

            <Cloud scale = { 0.9 } top = { windowHeight * .15 } left = { windowWidth * .8 } />
            <Cloud scale = { 1.2 } top = { windowHeight * .28 } left = { windowWidth * .65 } />
            <Cloud scale = { 0.7 } top = { windowHeight * .42 } left = { windowWidth * .7 } />
            <Cloud scale = { 1.6 } top = { windowHeight * .55 } left = { windowWidth * .75 } />
            <Cloud scale = { 1.2 } top = { windowHeight * .72 } left = { windowWidth * .8 } />
            <Cloud scale = { .8 } top = { windowHeight * .85 } left = { windowWidth * 0.85 } />

        </View>
    )
}

export default Clouds ;