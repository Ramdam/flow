import React from 'react' ;
import { StyleSheet, View } from 'react-native' ;
import { Surface, Text, Button } from 'react-native-paper' ;

const Option = ( { title, selection, description, onChange }) => {

    return (
        <Surface style = { styles.optionContainer }>
            <Text style = { styles.optionTitle} >{title}</Text>
            <View style = { styles.optionButtonContainer } ><Button mode = 'outlined' color = "#7dbbb9" contentStyle = { styles.optionButtonContent } style = { styles.optionButton }  labelStyle = { styles.optionButtonLabel } onPress = { () => onChange() }  >Modifier</Button></View>
            <Text style = { styles.optionSelection} >{selection}</Text>
            <Text style = { styles.optionDescription} >{description}</Text>
        </Surface>
    ) ;

}

const styles = StyleSheet.create({

    optionContainer : {
        width : '100%',
        height : 70,
        backgroundColor : 'transparent',
        flexDirection : 'row',
        flexWrap : 'wrap',
        justifyContent : 'space-between',
        alignItems : 'flex-start',
    },

    optionTitle : {
        width : '45%',
        height : '45%',
        paddingTop : 5,
        fontFamily : 'Raleway',
        fontWeight : 'bold',
        fontSize : 14,
        lineHeight: 16,
        color: '#000000',
        textAlign : 'left',
    },

    optionButtonContainer : {
        width : '35%',
        height : '40%',
        margin : 0,
        padding : 0,
        marginLeft : 30,
        marginTop : 2,
    },

    optionButtonContent : {
        margin : 0,
        padding : 0,
    },

    optionButton : {
        margin : 0,
        padding : 0,
        borderRadius : 20,
        borderWidth : .5,
        borderColor : '#7dbbb9',
    },

    optionButtonLabel : {
        color : "#7dbbb9",
        fontSize : 10,
        lineHeight : 10,
        padding : 0,
    },

    optionSelection : {
        width : '25%',
        height : '45%',
        fontFamily : 'Raleway',
        fontSize : 12,
        lineHeight: 14,
        color: '#3b3b3b',
        textAlign : 'left',
    },
     
    optionDescription : {
        width : '75%',
        height : '45%',
        fontFamily : 'Raleway',
        fontSize : 12,
        lineHeight: 14,
        color: '#3b3b3b',
        textAlign : 'right',
    },

}) ;

export default Option ;