import React, { useEffect, useState } from 'react' ;
import { Svg, Circle, G } from 'react-native-svg' ;

const ProgressBar = props => {
    
    const [ offset, setOffset ] = useState(0);
    
    const { 
        size, 
        progress, 
        strokeWidth, 
        circleOneStroke, 
        circleTwoStroke,
        children
    } = props ;

    const center = size / 2 ;
    const radius = size / 2 - strokeWidth / 2 ;
    const circumference = 2 * Math.PI * radius ;
    var progressState = progress * 100 ;

    useEffect(() => {
        progressState = progress * 100 ;
        const progressOffset = ((100 - progressState) / 100) * circumference ;
        setOffset(progressOffset) ;
        //circleRef.current.style = 'transition: stroke-dashoffset 850ms ease-in-out;';
    }, [setOffset, circumference, progress, offset]);

    return (
        <>
            <Svg
                width = { size }
                height = { size }
            >
                    <Circle
                        stroke = { circleOneStroke }
                        cx = { center }
                        cy = { center }
                        r = { radius }
                        strokeWidth = { strokeWidth }
                    />
                    <G transform = {'translate(-0,' + size + ')'}>
                        <G transform = "rotate(-90)">
                            <Circle
                                stroke = { circleTwoStroke }
                                cx = { center }
                                cy = { center }
                                r = { radius }
                                strokeWidth = { strokeWidth }
                                strokeDasharray = { circumference }
                                strokeDashoffset = { offset }
                                strokeLinecap = "round"
                            />
                        </G>
                    </G>
                    {children}
            </Svg>
        </>
    )
}

export default ProgressBar;