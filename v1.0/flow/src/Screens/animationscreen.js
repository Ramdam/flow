import React, { useEffect, useState } from 'react' ;
import { SafeAreaView, Animated, StyleSheet, Image } from 'react-native' ;
import { Surface, Button, IconButton, Text } from 'react-native-paper' ;
import LottieView from 'lottie-react-native' ;
import { LinearProgress } from 'react-native-elements' ;

import AsyncStorage from '@react-native-async-storage/async-storage' ;

import animationLotus from '../Assets/lotties/lotus.json' ;
import animationMandalas from '../Assets/lotties/mandalas.json' ;
import animationRond from '../Assets/lotties/rond.json' ;
import logo_flow from '../Assets/sprites/logo_flow.png' ;

import soundon_icon from '../Assets/sprites/soundon.png' ;
import soundoff_icon from '../Assets/sprites/soundoff.png' ;

import starSprite from '../Assets/sprites/star.png' ;
import { PopulateStartIntervals } from '../Components/utils.js' ;

const AnimationScreen = ( { route, navigation } ) => {

    const Animations = [ animationLotus, animationMandalas, animationRond ] ;

    const [ nextRoute, setNextRoute ] = useState('MoodboardScreen') ;
    const duration = route.params.duration ;
    const animation = route.params.animation ;
    const audioTrack = route.params.audio ;
    const mode = route.params.mode ;

    const [ soundIsOn, setSoundIsOn ] = useState(false) ;
    const [ starsCounter, setStarsCounter ] = useState(0) ;
    const [ starsIntervals, setStarsIntervals ] = useState([]) ;

    const [ animationIteration, setAnimationIteration ] = useState(0) ;
    const [ consigne, setConsigne ] = useState('Inspirez') ;

    const [ totalTime, setTotalTime] = useState(1000000) ;
    const [ remainingTime, setRemainingTime] = useState(1000000) ;

    const [ animationState, setAnimationState ] = useState( new Animated.Value(0) ) ;
    const [ fontSize, setFontSize ] = useState( new Animated.Value(0) ) ;

    const [ breathingIn, setBreathingIn ] = useState(route.params.mode === 0 ? 3000 : route.params.mode === 1 ? 5000 : 7000) ;
    const [ breathingOut, setBreathingOut ] = useState(route.params.mode === 0 ? 7000 : route.params.mode === 1 ? 5000 : 3000) ;

    const toggleSound = () => {
        // do some stuffs
        setSoundIsOn(!soundIsOn) ;
    }

    const Exit = () => {
        // save data
        navigation.navigate(nextRoute) ;
    }

    useEffect( () => {

        if (remainingTime !== 1000000)
        {
            let isCancelled = false ;

            const computeDuration = () => {
                setTimeout( () => {

                    !isCancelled && setRemainingTime(remainingTime - 500);
                    UpdateStars() ;
                }, 500) ;
            }
            if (remainingTime > 0)
                computeDuration() ;
            else
                Exit()

            return () => {
                isCancelled = true;
            };
        }

    }, [remainingTime])


    const Inspire = () => {

        setConsigne("Inspirez") ;
        Animated.timing(animationState, {
            useNativeDriver: false,
            toValue: .5,
            duration: breathingIn - 200
        }).start() ;
        Animated.timing(fontSize, {
            useNativeDriver: false,
            toValue: 1,
            duration: breathingIn - 200
        }).start() ;
    }

    const Expire = () => {

        setConsigne("Expirez") ;
        Animated.timing(animationState, {
            useNativeDriver: false,
            toValue: 1,
            duration: breathingOut - 200
        }).start();

        Animated.timing(fontSize, {
            useNativeDriver: false,
            toValue: 0,
            duration: breathingOut - 200
        }).start() ;
    }

    useEffect (() => {

        if (remainingTime !== 1000000)
        {
            let isCancelled = false ;

            const computeAnimation = () => {
                Inspire() ;

                setTimeout( () => {
                    Expire() ;
                }, breathingIn) ;

            setTimeout( () => {
                !isCancelled && setAnimationIteration(animationIteration+1) ;
                Animated.timing(animationState, {
                    useNativeDriver: false,
                    toValue: 0,
                    duration: 1
                    }).start();
                }, 10000) ;
            }

            if (remainingTime > 0)
                computeAnimation() ;

            return () => {
                isCancelled = true;
            };
        }

    }, [animationIteration]) ;

    useEffect ( () => {

        setStarsIntervals(PopulateStartIntervals()) ;
        setTotalTime(duration * 1000) ;
        setRemainingTime(duration * 1000) ;
        setAnimationIteration(1) ;

        AsyncStorage.getItem('@moodboardredirection')
        .then( value => {
            if (value === 'off')
                setNextRoute('HomeScreen')
            else if (value !== 'on')
                AsyncStorage.setItem('@moodboardredirection', 'on')

        })
        .catch( error => {
            console.log('error writing local storage', error) ;
        })

    }, [])

    const UpdateStars = () => {

        let elapsed = (totalTime - remainingTime)/1000 ;
        if (elapsed > 0)
        {
            let _stars = 0 ;
            starsIntervals.forEach( item => {
                if ( item.time <= elapsed)
                    _stars = item.stars ;
                else
                    return ;
            })
            setStarsCounter(_stars) ;
        }
    }

    useEffect( () => {

        console.log(starsCounter) ;
    }, [starsCounter])


    return (

        <SafeAreaView style = { styles.container }>

            <IconButton
                icon = { soundIsOn ? soundon_icon : soundoff_icon }
                color = '#7DBBB9'
                size = { 32 }
                compact = { true }
                animated = { true }
                onPress = { () => toggleSound() }
                style = { styles.soundIcon }
            />

            <Surface style = { styles.header }>
                <Surface style = { styles.appNameContainer }>
                    <Image source = { logo_flow } style = { styles.appName } />
                </Surface>
                <Surface style = { styles.starsContainer }>
                    <Image source = { starSprite } style = { styles.starIcon } />
                    <Text style = { styles.starsCounter }>{starsCounter}/50</Text>
                </Surface>
            </Surface>


            <Surface style = { styles.mainContainer}>
                <LottieView
                    style = {styles.lotties}
                    source = {Animations[animation]}
                    progress = {animationState}
                />

                <Animated.Text style = { [styles.consigne, { fontSize : fontSize.interpolate({
                    inputRange:[0,1],
                    outputRange : [8,32] })
                }]}>{consigne}</Animated.Text>

                <Surface style = { styles.actionButtons }>
                    <Button
                        mode = 'contained'
                        color = "#7dbbb9"
                        contentStyle = { styles.animationButtonContent }
                        style = { styles.animationButton }
                        labelStyle = { styles.animationButtonLabel } 
                        onPress = { () => Exit()}
                    >
                        Quitter
                    </Button>
                </Surface>
    
            </Surface>

            <Surface style = { styles.linearProgressView }>
                <LinearProgress
                    variant = 'determinate'
                    color = '#7dbbb9'
                    trackColor = '#7dbbb919'
                    style = { styles.linearProgressBar }
                    value = { ( totalTime - remainingTime)/totalTime }
                />
            </Surface>

        </SafeAreaView>

    ) ;
}

const styles = StyleSheet.create({

    container: {
        flex: 1,
        backgroundColor: '#e5e5e5',
        alignItems: 'center',
        justifyContent: 'center',
        padding : 0,
        margin : 0,
    },

    soundIcon : {
        position : 'absolute',
        top : 5,
        right : 5,
        backgroundColor : 'transparent',
        zIndex : 5
    },


    header : {
        width : '100%',
        flex : 1,
        display : 'flex',
        flexDirection : 'column',
        justifyContent : 'center',
        alignItems : 'center',
        backgroundColor : 'transparent',
    },

    appNameContainer : {

        flex : 1,
        width : '100%',
        flexDirection : 'column',
        justifyContent : 'center',
        alignItems : 'center',
        backgroundColor : 'transparent',
        marginTop : 40,
    },

    appName : {
        width : '30%',
        resizeMode : 'contain',
    },

    starsContainer : {

        flex : 1,
        width : '100%',
        flexDirection : 'row',
        justifyContent : 'center',
        alignItems : 'center',
        backgroundColor : 'transparent',

    },

    starIcon : {

        backgroundColor : 'transparent',
        width : 24,
        height : 24,

    },

    starsCounter : {
        fontFamily : 'Raleway',
        fontSize : 18,
        lineHeight : 36,
        textAlign : 'center',
        color : '#2a5d68',
    },

    mainContainer : {
        width : '100%',
        flex : 4,
        marginTop : '5%',
        justifyContent : 'space-evenly',
        alignItems : 'center',
        backgroundColor : 'transparent',
    },

    lotties : {
        width : 300,
        height : 300,
    },

    consigne : {
        marginBottom : 80,
        fontFamily : 'Raleway',
        fontSize : 18,
        lineHeight : 36,
        textAlign : 'center',
        color : '#2a5d68',
    },

    actionButtons : {
        width : '100%',
        height : 80,
        alignItems : 'center',
        justifyContent : 'center',
        backgroundColor : 'transparent',
    },

    animationButtonContent : {
        margin : 0,
        padding : 0,
    },

    animationButton : {
        margin : 0,
        padding : 0,
        marginTop : 40,
        borderRadius : 20,
        borderWidth : .5,
        borderColor : '#7dbbb9',
        width : 160,
        height : 40,
        alignSelf : 'center',
    },

    animationButtonLabel : {
        color : "#ffffff",
    },

    linearProgressView : {
        alignSelf : 'center',
        width : '100%',
        height : 40,
        justifyContent : 'flex-end',
        backgroundColor : 'transparent',
    },

    linearProgressBar : {
        height : 20,
    }
});


export default AnimationScreen ;