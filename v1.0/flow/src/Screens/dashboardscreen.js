import React, { useState, useEffect } from 'react' ;
import { StyleSheet } from 'react-native' ;
import { Surface, Text } from 'react-native-paper' ;
import Toggle from 'react-native-toggle-element' ;

import LottieView from 'lottie-react-native' ;

import animationWoman from '../Assets/lotties/yogi_origin.json' ;

const DashboardOfTheDay = ( { data } ) => {

    return (
        <>
            <Surface style = { styles.moduleStatsContainer }>
                <Text>Days</Text>
            </Surface>

            <Surface style = { styles.moodboardContainer}>

            </Surface>
        </>
    )
}

const DashboardOfTheWeek = ( { data } ) => {

    return (
        <>
            <Surface style = { styles.moduleStatsContainer }>
                <Text>Weeks</Text>
            </Surface>

            <Surface style = { styles.moodboardContainer}>

            </Surface>
        </>
    )
}













const DashboardScreen = ( { navigation }) => {

    const fakeData = [
        { timestamp : 'monday', value : 2.8 },
        { timestamp : 'wednesday', value : 3.4 },
        { timestamp : 'thursday', value : 4 },
        { timestamp : 'friday', value : 2.4 },
        { timestamp : 'saturday', value : 3.8 },
        { timestamp : 'sunday', value : 4.2 },
    ]

    const [ numberOfSessions, setNumberOfSessions ] = useState(3) ;
    const [ preferredMode, setPreferredMode ] = useState("Calme") ;

    const [toggleValue, setToggleValue] = useState(false) ;


    return (

        <Surface style = { styles.container }>
            <Surface style = { styles.header }>
                <Surface style = { styles.headerAnimationContainer }>
                    <LottieView
                        source = { animationWoman }
                        autoPlay
                        loop
                    />
                </Surface>
                
                <Text style = { styles.headerTitle }>Tableau de bord</Text>

                <Toggle
                    value = { toggleValue }
                    onPress = { (newState) => setToggleValue(newState) }
                    containerStyle = { styles.toggleContainer }
                    trackBarStyle = {{
                        height : 30,
                    }}
                    trackBar = {{
                        width : 130,
                        radius : 2,
                        activeBackgroundColor : '#7dbbb9',
                        inActiveBackgroundColor : '#7dbbb9',
                    }}
                    thumbButton = {{
                        width: 60,
                        height: 36,
                        radius: 2,
                        activeBackgroundColor : '##2a5d68',
                        inActiveBackgroundColor : '#2a5d68'
                    }}
                    leftComponent = {<Text style = {{ fontSize : 12, color : 'white' }}>Journée</Text>}
                    rightComponent = {<Text style = {{ fontSize : 12, color : 'white' }}>Semaine</Text>}
                />

            </Surface>
            
           
            {
                toggleValue ? <DashboardOfTheWeek /> : <DashboardOfTheDay />
            }

        </Surface>
    )

}

const styles = StyleSheet.create({

    container : {
        flex : 1,
        backgroundColor : '#E5E5E5',
        justifyContent : 'space-between',
    },

    header : {
        height : '30%',
        width : '100%',             
        backgroundColor : 'transparent',
        marginVertical : 20,
    },

    headerAnimationContainer : {
        width : '30%',
        height : '50%',
        backgroundColor : 'transparent',
        justifyContent : 'flex-start',
    },

    headerTitle : {
        fontFamily : 'Raleway',
        fontSize : 24,
        lineHeight : 28,
        fontWeight : '600',
        color : '#2A5D68',
        marginLeft : '8%',
    },

    toggleContainer : {

        width : '100%',
        height : '25%',
        flexDirection : 'column',
        justifyContent : 'flex-end',
        alignItems : 'center',

    },

    moduleStatsContainer : {
        height : '45%',
        backgroundColor : 'transparent',
        justifyContent : 'space-between',
    },

    moduleTitle : {
        fontFamily : 'Raleway',
        fontSize : 24,
        lineHeight : 28,
        fontWeight : 'bold',
        color : '#2A5D68',
        marginLeft : '8%',
        marginTop : 20,
    },

    moduleContainer : {
        width : '100%',
        height : '80%',
        backgroundColor : 'transparent',
        flexDirection : 'row',
    },

    radialProgress : {

        backgroundColor : 'transparent',
        width : '50%',
        height : '100%',
    },

    dataDescriptionContainer : {

        backgroundColor : 'transparent',
        width : '50%',
        flexDirection : 'column',

    },
    dataDescriptionItem : {
        backgroundColor : 'transparent',
        width : '100%',
        flexDirection : 'column',
        marginVertical : 20,
    },

    dataDescriptionItemTitle : {

        fontFamily : 'Raleway',
        fontSize : 16,
        lineHeight : 19,
        fontWeight : '300',
        color : '#000000',
        
    },
    dataDescriptionItemContent : {

        fontFamily : 'Raleway',
        fontSize : 24,
        lineHeight : 28,
        fontWeight : '500',
        color : '#000000',

    },

    moodboardContainer : {
        height : '35%',
        backgroundColor : '#ffffff',
        borderTopLeftRadius : 20,
        borderTopRightRadius : 20,
    },
    moodboardLineChart : { },

})

export default DashboardScreen ;



/*
        <Surface style = { styles.container }>
            <Surface style = { styles.header }>
                <Surface style = { styles.headerAnimationContainer }>
                    <LottieView
                        source = { animationWoman }
                        autoPlay
                        loop
                    />
                </Surface>
                <Text style = { styles.headerTitle }>Tableau de bord</Text>
            </Surface>

            <Surface style = { styles.moduleStatsContainer }>

                <Text style = { styles.moduleTitle }>Activité</Text>

                <Surface style = { styles.moduleContainer }>

                    <Surface style = { styles.radialProgress }>

                    </Surface>
                    <Surface style = { styles.dataDescriptionContainer }>

                        <Surface style = { styles.dataDescriptionItem }>
                            <Text style = { styles.dataDescriptionItemTitle }>Séances réalisées</Text>
                            <Text style = { styles.dataDescriptionItemContent }>{numberOfSessions}</Text>
                        </Surface>

                        <Surface style = { styles.dataDescriptionItem }>
                            <Text style = { styles.dataDescriptionItemTitle }>Mode favori</Text>
                            <Text style = { styles.dataDescriptionItemContent }>{preferredMode}</Text>
                        </Surface>

                    </Surface>
                </Surface>
            </Surface>

            
            <Surface style = { styles.moodboardContainer}>
            
                <Text style = { styles.moduleTitle }>Humeur</Text>
            
                <Surface style = { styles.moodboardLineChart}>
                </Surface>                 
            
            </Surface>

        </Surface>
*/