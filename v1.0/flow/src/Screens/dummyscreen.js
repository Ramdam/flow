import React from 'react' ;
import { StyleSheet } from 'react-native' ;
import { Surface, Text } from 'react-native-paper';

const DummyScreen = ( { navigation }) => {


    return (
        <Surface style = { styles.container }>
            <Text>Coucou</Text>
        </Surface>
    )

}

const styles = StyleSheet.create({

    container : {
        flex : 1,
        justifyContent : 'center',
        alignItems : 'center',
        elevation : 4,
        width : '90%',
        height : '90%',
        position : 'absolute',
        left : '5%',
        top : '5%',
    }


})

export default DummyScreen ;