import React, { useEffect } from 'react' ;
import { View, StyleSheet, Image } from 'react-native' ;
import AsyncStorage from '@react-native-async-storage/async-storage' ;
import LottieView from 'lottie-react-native' ;

import logo_ullo from '../Assets/sprites/logo_ullo.png' ;
import logo_flow from '../Assets/sprites/logo_flow.png' ;
import animation from '../Assets/lotties/yogi_origin.json' ;
import Clouds from '../Components/clouds.js' ;

const SplashScreen = ( { navigation } ) => {

    useEffect( async () => {

        var firstTime = true ;
        try
        {
            const value = await AsyncStorage.getItem('@firsttime') ;
            if( value !== null )
            {
              firstTime = (value === 'false' ? false : true) ;
            }
            setTimeout( () => {
                if (firstTime === false)
                    navigation.navigate('HomeScreen') ;
                else
                    navigation.navigate('OnboardingScreen') ;
            }, 5000) ;
        }
        catch( error )
        {
            console.log('error reading local storage', error) ;
            setTimeout( () => {
                navigation.navigate('HomeScreen') ;
            }, 5000)
        }

    }, [])
   
    return (

        <View style = { styles.background }>

            <Clouds />

            <View style = { styles.containerLogo}>
                <Image source = { logo_ullo } style = { styles.logo } />
            </View>

            <View style = { styles.containerAnimation}>
                <LottieView
                    source = { animation }
                    autoPlay
                    loop
                />
            </View>

            <View style = { styles.containerTitle}>
                <Image source = { logo_flow } style = { styles.title } />
            </View>

        </View>
    )
}

const styles = StyleSheet.create( {

    background : {
        width : '100%',
        flex : 1,
        backgroundColor : '#ffffff',
    },

    containerLogo : {
        width : '100%',
        flex : 1,
        justifyContent : 'center',
        alignItems : 'center',
    },

    logo : {
        width : '20%',
        resizeMode : 'contain',
    },

    containerAnimation : {
        flex : 2,
        width : '100%',
        justifyContent : 'center',
        alignItems : 'center',
    },

    animation : {
        width : '40%',
    },

    containerTitle : 
    {
        flex : 1,
        width : '100%',
        justifyContent : 'center',
        alignItems : 'center',
    },

    title : {
        width : '30%',
        resizeMode : 'contain',
    },

})

export default SplashScreen ;