import React from 'react' ;
import Swiper from 'react-native-swiper' ;
import AsyncStorage from '@react-native-async-storage/async-storage' ;

import { FirstScreen, SecondScreen, ThirdScreen } from '../Components/onboardingscreens.js' ;

const OnboardingScreen = ( { navigation } ) => {

    const beginJourney = () => {

        AsyncStorage.setItem('@firsttime', 'false')
            .then( () => { navigation.navigate('HomeScreen') ; })
            .catch( error => {
                console.log('error writing local storage', error) ;
                navigation.navigate('HomeScreen') ;
            })
    }

    return (

        <Swiper
            showsButtons = { false }
            dotColor = "#7dbbb9"
            activeDotColor = "#2a5d68"
        >

            <FirstScreen />
            <SecondScreen />
            <ThirdScreen onPressButton = { beginJourney }/>

        </Swiper>

    )
}

export default OnboardingScreen ;