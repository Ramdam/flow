import React, { useState, useEffect } from 'react' ;
import { StyleSheet } from 'react-native' ;
import { Surface, Text, IconButton, Button, Checkbox } from 'react-native-paper' ;
import LottieView from 'lottie-react-native' ;
import Moodboard from '../Components/moodboard.js' ;

import animationWoman from '../Assets/lotties/yogi_origin.json' ;


const MoodboardScreen = ( { route, navigation }) => {

    const [ checked, setChecked ] = useState(false) ;
    const ToggleCheckbox = () => {
        AsyncStorage.setItem('@moodboardredirection', checked === false ? 'off' : 'on') ;
        setChecked(!checked) ;
    }

    const [ description, setDescription ] = useState('Some description') ;

    const RecordMoodState = ( index ) => {
        // get the database
        // get the user doc
        // check subcollection session moodstates
        // check doc with year and week number
        // on the current day array, add value with timestamp
    }

    const SaveAndQuit = () => {



        navigation.navigate('HomeScreen') ;
    }

    return (
        <Surface style = { styles.container }>

            <Surface style = { styles.header }>

                <Surface style = { styles.animationWoman }>
                    <LottieView
                        source = { animationWoman }
                        autoPlay
                        loop
                    />
                </Surface>

                <Text style = { styles.description }>{description}</Text>

            </Surface>

            <Surface style = { styles.mainContent }>
                <Text style = { styles.moodboardTitle }>Comment vous sentez-vous à présent ?</Text>
                <Surface style = { styles.moodboardContainer }>
                    <Moodboard initialState = {2} onSelectionChange = { ( index ) => { console.log(index) ; RecordMoodState(index) ; }} />
                </Surface>
            </Surface>

            <Surface style = { styles.footer }>

                <Surface style = { styles.checkboxContainer }>
                    <Checkbox
                        status = { checked ? 'checked' : 'unchecked' }
                        onPress = { () => {
                            ToggleCheckbox() ;
                        }}
                        uncheckedColor = '#aeaeae'
                        color = '#7DBBB9'
                        label = "coucou"
                    />
                    <Text style = { styles.checkboxLabel }>Ne plus me le demander</Text>
                </Surface>

                <Button
                    mode = 'contained'
                    color = "#7dbbb9"
                    contentStyle = { styles.buttonContent }
                    style = { styles.button }
                    labelStyle = { styles.buttonLabel } 
                    onPress = { () => SaveAndQuit() }
                >
                    Revenir au menu
               </Button>

            </Surface>


        </Surface>


    )
}


const styles = StyleSheet.create({

    container : {
        flex : 1,
        width : '100%',
        justifyContent : 'center',
        alignItems : 'center',
    },

    header : {
        width : '100%',
        flex : 1,
        justifyContent : 'center',
        alignItems : 'center',
    },

    animationWoman : {
        flex : 3,
        width : '100%',
    },

    description : {
        flex : 1,
        width : '80%',
        textAlign : 'center',
        fontFamily : 'Raleway',
        fontSize : 12,
        lineHeight : 14,
        color : '#3B3B3B',
    },

    mainContent : {
        width : '100%',
        flex : 1,
        justifyContent : 'center',
        alignItems : 'center',
    },

    moodboardTitle : {
        width : '90%',
        textAlign : 'left',
        fontFamily : 'Raleway',
        fontWeight : 'bold',
        fontSize : 24,
        lineHeight : 28,
        color : '#2a5d68',
    },

    moodboardContainer : {
        marginTop : 40,
        width : '90%',
        height : '25%',
        backgroundColor : '#f3f3f3',
        borderRadius : 10,
        elevation : 10,
    },
    
    footer : {
        width : '100%',
        flex : 1,
        justifyContent : 'flex-end',
        alignItems : 'center',
    },

    checkboxContainer : {

        flexDirection : 'row',
        justifyContent : 'space-evenly',
        alignItems : 'center',
        marginBottom : 20,
    },

    checkboxLabel : {
        fontFamily : 'Raleway',
        fontSize : 12,
        lineHeight : 14,
        color : '#3B3B3B',
    },

    buttonContent : {



    },
    button : {
        marginBottom : 40,
        borderRadius : 20,
    },
    buttonLabel : {
        fontFamily : 'Raleway',
        fontSize : 18,
        lineHeight : 21,
        color : '#ffffff',
    },

})

export default MoodboardScreen ;