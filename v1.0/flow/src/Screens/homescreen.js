import React, { useState, useEffect } from 'react' ;
import { StyleSheet, ScrollView } from 'react-native' ;
import { Surface, Text, IconButton, Button } from 'react-native-paper' ;
import LottieView from 'lottie-react-native' ;
import Swiper from 'react-native-swiper' ;
import Icon from 'react-native-vector-icons/FontAwesome' ;

import MyModal from '../Components/mymodal.js' ;
import ModeModalContent from '../Components/modemodalcontent.js' ;
import DurationModalContent from '../Components/durationmodalcontent.js' ;
import AudioModalContent from '../Components/audiomodalcomponent.js' ;
import Moodboard from '../Components/moodboard.js' ;
import Option from '../Components/option.js' ;
import ProgressBar from '../Components/progress.js' ;

import animationWoman from '../Assets/lotties/yogi_origin.json' ;
import animationLotus from '../Assets/lotties/lotus.json' ;
import animationMandalas from '../Assets/lotties/mandalas.json' ;
import animationRond from '../Assets/lotties/rond.json' ;

import SettingsIcon from '../Assets/sprites/settings_icon.png' ;
import DashboardIcon from '../Assets/sprites/dashboard_icon.png' ;

import { useSelector } from 'react-redux' ;
import { useFirestoreConnect } from 'react-redux-firebase' ;

const HomeScreen = ( { navigation }) => {

    const [ username, setUsername ] = useState('friend') ;
    const { uid } = useSelector( state => state.firebase.auth ) ;

    const [ animationIndex, setAnimationIndex ] = useState(0) ;

    const [ modeModalIsVisible, setModeModalIsVisible ] = useState(false) ;
    const [ durationModalIsVisible, setDurationModalIsVisible ] = useState(false) ;
    const [ audioModalIsVisible, setAudioModalIsVisible ] = useState(false) ;

    const [ modeIndex, setModeIndex ] = useState(0) ;
    const [ durationValue, setDurationValue ] = useState(5) ;
    const [ audioIndex, setAudioIndex ] = useState(0) ;

    const [ totalNumberSessions, setTotalNumberSessions ] = useState(4) ;
    const [ preferredModeIndex, setPreferredModeIndex ] = useState(0) ;
    const [ timeElapsed, setTimeElapsed ] = useState(20) ;

    useEffect( () => {
        console.log(modeModalIsVisible) ;
    }, [modeModalIsVisible])

    useFirestoreConnect( uid && [{
      collection : 'users',
      doc : uid,
      storeAs : 'user',
    }]) ;

    useEffect( () => {

        UpdataDataviz() ;

    }, [])

    useEffect( () => {

        // check uid
        // if uid exists, get doc
        // from doc get nickname
        // update timestamp data

    }, [ uid ])

    useEffect( () => {

        console.log('animation index changed : ' + animationIndex) ;

    }, [ animationIndex])

    const GoToAnimation = () => {
        const payload = {
            animation : animationIndex,
            mode : modeIndex,
            duration : durationValue,
            audio : audioIndex,
        }
        navigation.navigate("AnimationScreen", payload) ;
    }

    const RecordMoodState = ( index ) => {
        // get the database
        // get the user doc
        // check subcollection session moodstates
        // check doc with year and week number
        // on the current day array, add value with timestamp
    }

    const UpdataDataviz = () => {

        // get total elapsed time of the day
        // get number of sessions of the day
        // get used menus
        // display infos
    }

    const modeOptions = [
        {
            title : 'Calme',
            description : '4 secs d\'inspiration, 6 secs d\'expiration.',
        },
        {
            title : 'Ancrage',
            description : '5 secs d\'inspiration, 5 secs d\'expiration.',
        },
        {
            title : 'Boost',
            description : '6 secs d\'inspiration, 4 secs d\'expiration.',
        }
    ]

    const audioOptions = [
        {
            title : 'Aucun',
            description : '',
        },
        {
            title : 'Océan',
            description : 'Laissez vous guider par le bruit des vagues.',
        },
        {
            title : 'Bols tibétains',
            description : 'Le chant des bols tibétains guidera votre respiration.',
        }
    ]

    return (
        <ScrollView contentContainerStyle = { styles.screenContainer }
        >
            <Surface style = { styles.settingsIcon  }>
                <IconButton
                    icon = { SettingsIcon }
                    color = '#7DBBB9'
                    size = { 28 }
                    compact = { true }
                    animated = { true }
                    onPress = { () => { navigation.navigate("SettingsScreen") } }
                />
            </Surface>

            <Surface style = { styles.header }>
                <LottieView
                    source = { animationWoman }
                    autoPlay
                    loop
                    style = { styles.headerAnimationWoman }
                />
                <Text style = { styles.headerGreetings }>Hello <Text style = { styles.headerGreetingsSpan }>{username} !</Text></Text>

                <Surface style = { styles.headerMoodBoard }>
                    <Text style = { styles.headerMoodboardTitle }>Mon humeur</Text>
                    <Moodboard initialState = {2} onSelectionChange = { ( index ) => { console.log(index) ; RecordMoodState(index) ; }} />
                </Surface>


            </Surface>

            <Surface style = { styles.content }>

                <Surface style = { styles.animationSelection }>
                    
                    <Text style = { styles.animationSelectionTitle }>Mon exercice de cohérence cardiaque</Text>
                    
                    <Surface style = { styles.animationSelectionContainer }>

                        <Swiper
                            showsButtons = { true }
                            dotColor = "#7dbbb9"
                            activeDotColor = "#2a5d68"
                            loop = { true }
                            showsPagination = { false }
                            nextButton = { <Icon name = "chevron-right" size = { 24 } color = "#7DBBB9" />}
                            prevButton = { <Icon name = "chevron-left" size = { 24 } color = "#7DBBB9" />}
                            onIndexChanged = { value => setAnimationIndex(value) }
                            style = { styles.animationSelectionWrapperSwiper }
                        >
                            
                            <LottieView
                                source = { animationMandalas }
                                autoPlay
                                loop
                                style = { styles.animationSelectionLotus }
                            />
                            
                            <LottieView
                                source = { animationLotus }
                                autoPlay
                                loop
                                style = { styles.animationSelectionLotus }
                            />
                            
                            <LottieView
                                source = { animationRond }
                                autoPlay
                                loop
                                style = { styles.animationSelectionLotus }
                            />

                        </Swiper>

                        <Surface style = { styles.animationOptionsContainer }>

                            <Option title = "Mode" selection = { modeOptions[modeIndex].title } description = { modeOptions[modeIndex].description } onChange = { () => { setModeModalIsVisible(true) } }/>
                            <Option title = "Durée" selection = { durationValue + " min"} description = "" onChange = { () => { setDurationModalIsVisible(true) } }/>
                            <Option title = "Guide sonore" selection = { audioOptions[audioIndex].title} description = "" onChange = { () => { setAudioModalIsVisible(true) } }/>

                        </Surface>

                        <Button
                            mode = 'contained'
                            color = "#7dbbb9"
                            contentStyle = { styles.animationButtonContent }
                            style = { styles.animationButton }
                            labelStyle = { styles.animationButtonLabel } 
                            onPress = { () => GoToAnimation() }
                        >
                            Commencer
                        </Button>

                    </Surface>

                </Surface>


                <Surface style = { styles.dashboardPreview }>
                    <Text style = { styles.dashboardPreviewTitle }>Mes données</Text>
                    <Surface style = { styles.dashboardPreviewContainer }>

                        <Surface style = { styles.dashboardItemLeftContainer }>
                            <Surface style = { styles.circularProgressContainer }>
                                <ProgressBar
                                    progress = { timeElapsed/30 > 1 ? 1 : timeElapsed/30 }
                                    size = { 100 }
                                    strokeWidth = { 8 }
                                    circleOneStroke = "#7dbbb97f"
                                    circleTwoStroke = "#7dbbb9"
                                >
                                    <Surface style = { styles.circularProgressContent }>
                                        <Text style = { styles.circularProgressTitle }>Temps de pratique</Text>
                                        <Text style = { styles.circularProgressValue }>{timeElapsed}m</Text>
                                    </Surface>  
                                </ProgressBar>
                            </Surface>
                        </Surface>

                        <Surface style = { styles.dashboardItemMiddleContainer }>
                            <Surface style = { styles.dashboardItem }>
                                <Text style = { styles.dashboardItemTitle }>Séances réalisées</Text>
                                <Text style = { styles.dashboardItemContent }>{totalNumberSessions}</Text>
                            </Surface>
                            <Surface style = { styles.dashboardItem }>
                                <Text style = { styles.dashboardItemTitle }>Mode favori</Text>
                                <Text style = { styles.dashboardItemContent }>{modeOptions[preferredModeIndex].title}</Text>
                            </Surface>
                        </Surface> 

                        <Surface style = { styles.dashboardItemRightContainer }>
                            <Surface style = { styles.dashboardIconContainer  }>
                                <IconButton
                                    icon = { DashboardIcon }
                                    color = '#7DBBB9'
                                    size = { 42 }
                                    compact = { true }
                                    animated = { true }
                                    onPress = { () => { navigation.navigate('DashboardScreen') } }
                                />
                            </Surface>
                        </Surface> 

                    </Surface>
                </Surface>

            </Surface>

            <MyModal visible = { modeModalIsVisible } hide = { () => setModeModalIsVisible(false) }>
                <ModeModalContent
                    onValidate = { ( value) => { setModeIndex(value) ; setModeModalIsVisible(false) } }
                    options = { modeOptions }
                    currentIndex = { modeIndex }
                />
            </MyModal>
            <MyModal visible = { durationModalIsVisible } hide = { () => setDurationModalIsVisible(false) }>
                <DurationModalContent
                    onValidate = { value => { setDurationValue(value) ; setDurationModalIsVisible(false) } }
                />
            </MyModal>
            <MyModal visible = { audioModalIsVisible } hide = { () => setAudioModalIsVisible(false) }>
                <AudioModalContent
                    onValidate = { ( value) => { setAudioIndex(value) ; setAudioModalIsVisible(false) } }
                    options = { audioOptions }
                    currentIndex = { audioIndex }                
                />
            </MyModal>

        </ScrollView>
    )
}


const styles = StyleSheet.create({

    screenContainer : {
        backgroundColor : '#e5e5e5',
        width : '100%',
    },

    settingsIcon : {
        backgroundColor : 'white',
        position : 'absolute',
        top : 20, 
        right : 20,
        borderRadius : 32,
        zIndex : 2,
        elevation : 5,
        padding : 0,
        margin : 0,
        width : 32,
        height : 32,
        justifyContent : 'center',
        alignItems : 'center',
    },
    
    header : {
        backgroundColor : 'transparent',
        width : '100%',
        height : 200,
        justifyContent : 'center',
    },

    headerAnimationWoman : {
        left : 5,
        width : 80,
    },

    headerGreetings : {
        alignSelf : 'center',
        left : 5,
        top : -45,
        fontFamily : 'Raleway',
        fontStyle : 'normal',
        fontWeight : '300',
        fontSize : 20,
        lineHeight: 23,
        color: '#2a5d68'
    },

    headerGreetingsSpan : {
        fontFamily : 'Raleway',
        fontStyle : 'normal',
        fontWeight : 'bold',
        fontSize : 20,
        lineHeight: 23,
        color: '#2a5d68'
    },

    headerMoodBoard : {
        backgroundColor : 'transparent',
        width : '100%',
        height : 60,
        marginBottom : 10,
    },

    headerMoodboardTitle : {
        left : 20,
        fontFamily : 'Raleway',
        fontStyle : 'normal',
        fontSize : 12,
        lineHeight: 14,
        color: '#3b3b3b',
    },

    content : {
        width : '100%',
        backgroundColor : '#ffffff',
        borderTopStartRadius : 20,
        borderTopEndRadius : 20,
    },
    
    animationSelection : {
        borderTopStartRadius : 20,
        borderTopEndRadius : 20,
    },

    animationSelectionTitle : {
        left : 20,
        top : 15,
        fontFamily : 'Raleway',
        fontStyle : 'normal',
        fontSize : 12,
        lineHeight: 14,
        color: '#3b3b3b',
    },

    animationSelectionContainer : {
        backgroundColor : '#f3f3f3',
        elevation : 2,
        marginHorizontal : 20,
        marginVertical : 30,
        padding : 10,
        borderRadius : 10,
        height : 360,
    },

    animationSelectionWrapperSwipper : {
        height : 120,
    },

    animationSelectionLotus : {
        alignSelf : 'center',
        height : 100,
    },

    animationOptionsContainer : {
        backgroundColor : 'transparent',
        justifyContent : 'space-evenly',
    },

    animationButtonContent : {
        margin : 0,
        padding : 0,
    },

    animationButton : {
        margin : 0,
        padding : 0,
        borderRadius : 20,
        borderWidth : .5,
        borderColor : '#7dbbb9',
        width : 160,
        alignSelf : 'center',
    },

    animationButtonLabel : {
        color : "#ffffff",
        fontSize : 10,
        lineHeight : 10,
    }, 

    dashboardPreview : {
        backgroundColor : 'transparent',
        width : '100%',
        height : 180,
    },

    dashboardPreviewTitle : {
        left : 20,
        top : 15,
        fontFamily : 'Raleway',
        fontStyle : 'normal',
        fontSize : 12,
        lineHeight: 14,
        color: '#3b3b3b',
    },

    dashboardPreviewContainer : {
        backgroundColor : '#f3f3f3',
        elevation : 2,
        marginHorizontal : '5%',
        marginVertical : 30,
        borderRadius : 10,
        height : 100,
        flexDirection : 'row',
        justifyContent : 'space-between',
        alignItems : 'center',
    },

    dashboardItemLeftContainer : {
        width : '33%',
        height : '100%',
        justifyContent : 'center',
        alignItems : 'center',
        backgroundColor : 'transparent',
    },

    dashboardItemMiddleContainer : {
        width : '50%',
        height : '100%',
        justifyContent : 'center',
        alignItems : 'center',
        backgroundColor : 'transparent',
    },

    dashboardItemRightContainer : {
        width : '16%',
        height : '100%',
        justifyContent : 'flex-end',
        alignItems : 'flex-end',
        backgroundColor : 'transparent',
    },

    circularProgressContainer : {
        justifyContent : 'space-between',
        alignItems : 'center',
        backgroundColor : 'transparent',
    },

    circularProgressContent : {
        backgroundColor : 'transparent',
        width : '80%',
        height : '80%',
        marginLeft : '10%',
        marginTop : '10%',
        justifyContent : 'center',
        alignItems : 'center',
    },
    circularProgressTitle : {
        fontFamily : 'Raleway',
        fontStyle : 'normal',
        fontSize : 12,
        lineHeight: 14,
        color: '#3b3b3b',
        textAlign : 'center',
        marginTop : 5,
    },

    circularProgressValue : {
        fontFamily : 'Raleway',
        fontStyle : 'normal',
        fontSize : 18,
        lineHeight: 21,
        color: '#000000',
        padding : 0,
        paddingTop : 5,
    },

    dashboardItem : {
        width : '80%',
        height : '40%',
        marginVertical : '2%',
        backgroundColor : 'transparent',
        justifyContent : 'center',
        alignItems : 'flex-start',
    },

    dashboardItemTitle : {
        margin : 0,
        padding : 0,
        fontFamily : 'Raleway',
        fontSize : 12,
        lineHeight : 14,
        color : '#000000',
    },

    dashboardItemContent : {
        margin : 0,
        padding : 0,
        fontFamily : 'Raleway',
        fontSize : 18,
        lineHeight : 21,
        color : '#000000',
    },
    
    dashboardIconContainer : {
        backgroundColor : 'white',
        borderRadius : 48,
        zIndex : 2,
        elevation : 5,
        width : 48,
        height : 48,
        marginRight : 5,
        marginBottom : 5,
        justifyContent : 'center',
        alignItems : 'center',
    },

})

export default HomeScreen ;