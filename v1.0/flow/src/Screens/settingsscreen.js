import React from 'react' ;
import { StyleSheet } from 'react-native' ;
import { Surface, Text, Button } from 'react-native-paper';

const SettingsScreen = ( { navigation }) => {


    return (
        <Surface style = { styles.container }>
            <Text>settings</Text>
            <Button mode="contained" onPress = {() => navigation.navigate("CrossroadScreen") }>Gérer son compte</Button>
        </Surface>
    )

}

const styles = StyleSheet.create({

    container : {
        flex : 1,
        justifyContent : 'space-evenly',
        alignItems : 'center',
        elevation : 4,
        width : '90%',
        height : '90%',
        position : 'absolute',
        left : '5%',
        top : '5%',
    }

})

export default SettingsScreen ;