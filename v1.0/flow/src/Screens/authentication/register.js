import React, { useState, useEffect } from 'react' ;
import { StyleSheet, Alert, TouchableOpacity, Image, View } from 'react-native' ;
import { Input } from 'react-native-elements' ;
import { Surface } from 'react-native-paper' ;
import { useFirebase } from "react-redux-firebase" ;
import Swiper from 'react-native-swiper' ;
import LottieView from 'lottie-react-native' ;

import animation from '../../Assets/lotties/yogi_origin.json' ;
import Clouds from '../../Components/clouds.js' ;
import navRightArrow from '../../Assets/sprites/nav-right-arrow.png' ;

const RegisterScreen = ( { navigation } ) => {

  const firebase = useFirebase();

  const [ email, setEmail ] = useState() ;
  const [ password, setPassword ] = useState() ;
  const [ nickname, setNickname ] = useState() ;

  const [ errorMessageEmail, setErrorMessageEmail ] = useState('') ;
  const [ errorMessagePassword, setErrorMessagePassword ] = useState('') ;
  const [ errorMessageNickname, setErrorMessageNickname ] = useState('') ;

  const [ pageIndex, setPageIndex ] = useState(0) ;

  useEffect( () => {
    setErrorMessageEmail('')
  }, [email])
  useEffect( () => {
    setErrorMessageNickname('')
  }, [nickname])
  useEffect( () => {
    setErrorMessagePassword('')
  }, [password])

  useEffect( () => {

    console.log(pageIndex)

    if (pageIndex === 1)
    {
      checkNicknameAndGo() ;
    }

    else if (pageIndex === 2)
    {
      checkEmailAndGo() ;
    }
  }, [pageIndex])


  const checkNicknameAndGo = () => {
    if ( nickname === '' || nickname === undefined )
    {
      setErrorMessageNickname("Vous devez indiquer un pseudo valide") ;
      setPageIndex(0) ;
    }
    else if (nickname.length < 2)
    {
      setErrorMessageNickname("Votre pseudo doit être composé d'au moins deux caractères") ;
      setPageIndex(0) ;
    }
    else
      setPageIndex(1) ;
  }

  const checkEmailAndGo = () => {
    if ( email === '' || email === undefined )
    {
      setErrorMessageEmail("Vous devez indiquer une adresse email valide") ;
      setPageIndex(1) ;
    }
    else
      setPageIndex(2) ;
  }

  const BeginSignupProcess = () => {

    if (nickname === undefined || nickname === "")
    {
      setErrorMessageNickname("Vous devez indiquer un pseudo valide") ;
      setPageIndex(0)
      return ;
    }
    if (email === undefined || email === "")
    {
      setErrorMessageEmail("Vous devez indiquer une adresse email valide") ;
      setPageIndex(1)
      return ;
    }
    if (password === undefined || password === "")
    {
      setErrorMessagePassword("Vous devez indiquer un mot de passe valide") ;
      return ;
    }

    createNewUser({
      email : email,
      password : password,
      username : nickname
    })
  }

  const createNewUser = ({ email, password, username }) => {

    firebase.createUser(
      { email, password },
      { username, email }
    )
    .then( () => {
      navigation.navigate('Home') ;
    })
    .catch( error => {
      console.log(error.message) ;
      if (error.message.includes('already in use'))
      {
        Alert.alert(
          "Création de compte",
          "L'adresse email est déjà associée à un autre compte. Souhaitez-vous vous connecter avec cette adresse ?",
          [
            { text: "OK", onPress: () => navigation.navigate('Login') },
            { text: "Cancel", onPress: () => setPageIndex(1) }
          ]
        );
      }
      else if (error.message.includes('badly formatted'))
      {
        Alert.alert(
          "Création de compte",
          "Le format de votre adresse email n'est pas valide.",
          [
            { text: "Ok", onPress: () => setPageIndex(1) }
          ]
        );
      }
      else if (error.message.includes(''))
      {
        console.log('') ;
      }
    }) ;
  };
  
  
  return (
        <Swiper
            showsButtons = { false }
            loop = { false }
            onIndexChanged = { ( index ) => { console.log(index)} }
            activeDotColor = '#2A5D68'
            index = { pageIndex }
            style = { styles.background }
        >
                
            <Surface style = { styles.slide }>

                <Clouds />

                <Surface style = { styles.containerAnimation}>
                    <LottieView
                        source = { animation }
                        autoPlay
                        loop
                    />
                </Surface>

                <Surface style = { styles.formContainer }>
                    <Input
                        label = 'Quel est votre nom ?'
                        labelStyle = { styles.inputLabel }
                        placeholder = 'Prénom Nom'
                        containerStyle = { styles.inputContainer}
                        inputStyle = { styles.input }
                        inputContainerStyle = { styles.inputInputContainer }
                        errorStyle = { styles.inputErrorStyle }
                        errorMessage = { errorMessageNickname }
                        clearButtonMode = 'while-editing'
                        keyboardType = 'default'
                        textContentType = 'nickname'
                        onChangeText = { value => setNickname(value)}
                        value = { nickname }
                        onPressIn = { () => { 'pressed in'} }
                    />
                </Surface>

                <Surface style = { styles.ctaContainer }>

                    <TouchableOpacity
                        onPress = { () => checkNicknameAndGo() }
                    >
                        <Image source = { navRightArrow } style = { styles.ctaImage } />
                    </TouchableOpacity>

                </Surface>

                <Surface style = { styles.bottom }></Surface>

            </Surface>

            <Surface style = { styles.slide }>

                <Clouds />
                
                <Surface style = { styles.containerAnimation}>
                    <LottieView
                        source = { animation }
                        autoPlay
                        loop
                    />
                </Surface>

                <Surface style = { styles.formContainer }>
                    <Input
                        label = 'Votre adresse email ?'
                        labelStyle = { styles.inputLabel }
                        placeholder = 'ada.lovelace@ullo.fr'
                        containerStyle = { styles.inputContainer}
                        inputStyle = { styles.input }
                        inputContainerStyle = { styles.inputInputContainer }
                        errorStyle = { styles.inputErrorStyle }
                        errorMessage = { errorMessageEmail }
                        clearButtonMode = 'while-editing'
                        keyboardType = 'email-address'
                        textContentType = 'emailAddress'
                        onChangeText = { value => setEmail(value)}
                        value = { email }        
                    />
                </Surface>

                <Surface style = { styles.ctaContainer }>

                    <TouchableOpacity
                        onPress = { () => checkEmailAndGo() }
                    >
                        <Image source = { navRightArrow } style = { styles.ctaImage } />
                    </TouchableOpacity>
                </Surface>

                <Surface style = { styles.bottom }></Surface>

            </Surface>

            <Surface style = { styles.slide }>

                <Clouds />

                <Surface style = { styles.containerAnimation}>
                    <LottieView
                        source = { animation }
                        autoPlay
                        loop
                    />
                </Surface>

                <Surface style = { styles.formContainer }>
                    <Input
                        label = 'Votre mot de passe ?'
                        labelStyle = { styles.inputLabel }
                        placeholder = '************'
                        containerStyle = { styles.inputContainer}
                        inputStyle = { styles.input }
                        inputContainerStyle = { styles.inputInputContainer }
                        errorStyle = { styles.inputErrorStyle }
                        errorMessage = { errorMessagePassword }
                        clearButtonMode = 'while-editing'
                        keyboardType = 'visible-password'
                        textContentType = 'password'
                        secureTextEntry = { true }
                        onChangeText = { value => setPassword(value)}
                        value = { password }        
                    />
                </Surface>

                <Surface style = { styles.ctaContainer }>
                    <TouchableOpacity
                        onPress = { () => BeginSignupProcess() }
                    >
                        <Image source = { navRightArrow } style = { styles.ctaImage } />
                    </TouchableOpacity>
                </Surface>

                <Surface style = { styles.bottom }></Surface>

            </Surface>

        </Swiper>
    )
}


const styles = StyleSheet.create({

    background : {
        backgroundColor : '#ffffff',
    },

    slide : {
        flex : 1,
        backgroundColor : '#ffffff',
        justifyContent : 'center',
        alignItems : 'center',
        backgroundColor : 'transparent',
    },

    containerAnimation : {
        flex : 2,
        width : '100%',
        justifyContent : 'center',
        alignItems : 'center',
        backgroundColor : 'transparent',
    },

    animation : {
        width : '60%',
    },

    formContainer : {
        flex : 3,
        width : '100%',
        justifyContent : 'center',
        alignItems: 'center',
        backgroundColor : 'transparent',
    },

    inputContainer : {
        width : '80%',
        height : 40,
        marginVertical : 35,
    },

    inputInputContainer : {
        backgroundColor : '#F3F3F3',
        height : 50,
        borderRadius: 10,
        borderBottomWidth : 0,
        paddingLeft : 10,
    },

    input : {
        fontFamily: 'Raleway',
        fontSize: 14,
        lineHeight: 17,
        color : '#2A5D68',
    },

    inputLabel : {
        paddingLeft : 10,
        fontFamily: 'Raleway',
        fontSize: 24,
        lineHeight: 28,
        color: '#2A5D68',
        marginBottom : 10,
    },

    inputErrorStyle : {
        color: '#7dbbb9',
    },

    ctaContainer : {
        flex : 1,
        width : '100%',
        justifyContent : 'flex-end',
        alignItems: 'flex-end',
        marginRight : 40,
        backgroundColor : 'transparent',
    },

    bottom : {
        flex : 0.5,
        backgroundColor : 'transparent',
    }
})


export default RegisterScreen ;