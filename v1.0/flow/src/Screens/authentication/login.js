import React, { useState, useEffect } from 'react' ;
import { Alert, StyleSheet } from 'react-native' ;
import { Surface, Button } from 'react-native-paper' ;
import { Input } from 'react-native-elements' ;
import LottieView from 'lottie-react-native';
import { useFirebase } from 'react-redux-firebase' ;
import { useNetInfo } from '@react-native-community/netinfo';

import animation from '../../Assets/lotties/yogi_origin.json' ;
import Clouds from '../../Components/clouds.js' ;


const LoginScreen = ( { navigation } ) => {

    const firebase = useFirebase() ;
    const netInfo = useNetInfo() ;

    const [ email, setEmail ] = useState() ;
    const [ password, setPassword ] = useState() ;  

    const [ errorMessageEmail, setErrorMessageEmail ] = useState('') ;
    const [ errorMessagePassword, setErrorMessagePassword ] = useState('') ;

    useEffect( () => {
        setErrorMessageEmail('')
    }, [email])
    useEffect( () => {
        setErrorMessagePassword('')
    }, [password])

    useEffect( () => {
      setTimeout( () => {
        if (netInfo.isInternetReachable === false)
        {
          Alert.alert(
            "Wifi status",
            "You do not seem connected to any network. You may experience some problems saving your progress and informations",
            [
              { text: "OK", onPress: () => console.log("OK Pressed") }
            ]
          )
        }
      }, 2000)
    })

    const beginLoginProcess = () => {

        if (netInfo.isInternetReachable === false)
        {
          Alert.alert(
            "Wifi status",
            "You do not seem connected to any network. You may experience some problems saving your progress and informations",
            [
              { text: "OK", onPress: () => console.log("OK Pressed") }
            ]
          )
          return ;
        }

        if (email === undefined || email === "")
        {
          setErrorMessageEmail("Vous devez indiquer une adresse email valide") ;
          return ;
        }    
        if (password === undefined || password === "")
        {
          setErrorMessagePassword("Vous devez indiquer un mot de passe valide") ;
          return ;
        }

        LoginFun()
    }

    const LoginFun = () => {

        firebase.login(
            {
                email : email,
                password : password
            }
        )
        .then( () => {
            navigation.navigate('SettingsScreen')
        })
        .catch( error => {
            console.log(error) ;
            Alert.alert(
              "Database error",
              error.message,
              [
                { text: "Got it", onPress: () => console.log("OK Pressed") }
              ]
            )
        })
    }

    return (
        <Surface style = { styles.background }>

            <Clouds />

            <Surface style = { styles.containerAnimation}>
                <LottieView
                    source = { animation }
                    autoPlay
                    loop
                />
            </Surface>

            <Surface style = { styles.formContainer }>

                <Input
                    label = 'Email'
                    labelStyle = { styles.inputLabel }
                    placeholder = 'ada.lovelace@ullo.fr'
                    containerStyle = { styles.inputContainer}
                    inputStyle = { styles.input }
                    inputContainerStyle = { styles.inputInputContainer }
                    errorStyle = { styles.inputErrorStyle }
                    errorMessage = { errorMessageEmail }
                    clearButtonMode = 'while-editing'
                    keyboardType = 'email-address'
                    textContentType = 'emailAddress'
                    onChangeText = { value => setEmail(value)}
                    value = { email }        
                />

                <Input
                    label = 'Mot de passe'
                    labelStyle = { styles.inputLabel }
                    placeholder = '************'
                    containerStyle = { styles.inputContainer}
                    inputStyle = { styles.input }
                    inputContainerStyle = { styles.inputInputContainer }
                    errorStyle = { styles.inputErrorStyle }
                    errorMessage = { errorMessagePassword }
                    clearButtonMode = 'while-editing'
                    textContentType = 'password'
                    secureTextEntry = { true }
                    onChangeText = { value => setPassword(value) }
                    value = { password }        
                />

                <Button
                    mode = 'text'
                    labelStyle = { styles.passwordRequestButtonLabel }
                    style = { [styles.passwordRequestButtonContainer, { marginTop : errorMessagePassword === '' ? 10 : 40, } ]}
                    onPress = { () => navigation.navigate('RequestPassword') }
                >
                    Mot de passe oublié ?
                </Button>

                <Button
                    mode = 'contained'
                    color = "#7dbbb9"
                    buttonStyle = { styles.button }
                    labelStyle = { [styles.buttonsLabel, { color : '#ffffff'}]  }
                    style = { styles.buttonContainer }
                    onPress = { () => beginLoginProcess() }
                >
                    Valider
                </Button>

                <Button
                    mode = 'outlined'
                    labelStyle = { styles.buttonsLabel }
                    style = { styles.buttonContainer }
                    onPress = { () => console.log('pressed') }
                >
                    Créer un nouveau compte
                </Button>

        
            </Surface>

           <Surface style = { styles.bottom }></Surface> 

        </Surface>
    )
}


const styles = StyleSheet.create( {

    background : {
        width : '100%',
        flex : 1,
        backgroundColor : '#ffffff',
        justifyContent : 'center',
        alignItems : 'center',
    },

    containerAnimation : {
        flex : 1,
        width : '100%',
        justifyContent : 'center',
        alignItems : 'center',
    },

    animation : {
        width : '40%',
    },

    formContainer : {
        flex : 4,
        width : '100%',
        justifyContent : 'flex-start',
        alignItems: 'center',
    },

    inputContainer : {
        width : '80%',
        height : 40,
        marginVertical : 35,
    },

    inputInputContainer : {
        backgroundColor : '#F3F3F3',
        height : 50,
        borderRadius: 10,
        borderBottomWidth : 0,
        paddingLeft : 2,
    },

    input : {
        fontFamily: 'Raleway',
        fontSize: 14,
        lineHeight: 17,
        color : '#2A5D68',
        paddingLeft : 10,

    },

    inputLabel : {
        paddingLeft : 10,
        marginBottom : 5,
        fontFamily: 'Raleway',
        fontSize: 24,
        lineHeight: 28,
        color: '#2A5D68',
    },

    inputErrorStyle : {
        color: '#7dbbb9',
    },

    buttonsLabel : {
        fontFamily: 'Raleway',
        fontSize: 14,
        lineHeight: 17,
        textAlign: 'center',
        color : '#2A5D68',
    },

    passwordRequestButton : {
        marginTop : 5,
        backgroundColor : 'transparent',
    },
    
    passwordRequestButtonLabel : {
        fontFamily: 'Raleway',
        fontSize: 10,
        lineHeight: 12,
        color : '#7DBBB9',
    },

    passwordRequestButtonContainer : {
        alignSelf : 'flex-start',
        textAlign : 'left',
        marginLeft : '10%',
        marginBottom : 45,
    },

    buttonContainer : {
        width : 280,
        marginVertical : 15,
        borderRadius : 5,
    },

    button : {
        backgroundColor : '#7DBBB9',
        borderRadius : 10,
    },

    buttonTitle : {
        fontFamily: 'Raleway',
        fontSize: 18,
        lineHeight: 21,
        textAlign: 'center',
        color : '#ffffff',
    },

    bottom : {
        flex : 0,
    }
})

export default LoginScreen ;