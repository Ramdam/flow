import React, { useEffect } from 'react' ;
import { Surface, Button } from 'react-native-paper' ;

import { StyleSheet, Image } from 'react-native' ;
import LottieView from 'lottie-react-native' ;

import logo_ullo from '../../Assets/sprites/logo_ullo.png' ;
import logo_flow from '../../Assets/sprites/logo_flow.png' ;
import animation from '../../Assets/lotties/yogi_origin.json' ;
import Clouds from '../../Components/clouds.js' ;

const CrossroadScreen = ( { navigation }) => {

    return (
        <Surface style = { styles.background }>

            <Clouds />

            <Surface style = { styles.logoContainer }>
                <Image source = { logo_ullo } style = { styles.logo } />
            </Surface>

            <Surface style = { styles.animationContainer }>
                <LottieView
                    source = { animation }
                    autoPlay
                    loop
                />
            </Surface>

            <Surface style = { styles.titleContainer }>
                <Image source = { logo_flow } style = { styles.title } />
            </Surface>

            <Surface style = { styles.buttonsContainer }>
                <Button
                    mode = 'outlined'
                    color = "#2a5d68"
                    style = { [styles.buttonContainer, { backgroundColor : '#f3f3f3' } ] }
                    labelStyle = { styles.buttonsLabel }
                    onPress = { () => { navigation.navigate('LoginScreen') } }
                >
                    Se connecter
                </Button>
                <Button
                    mode = 'contained'
                    color = "#7dbbb9"
                    style = { styles.buttonContainer }
                    labelStyle = { [styles.buttonsLabel, { color : '#ffffff'}]  }
                    onPress = { () => { navigation.navigate('RegisterScreen') } }
                >
                    S'inscrire
                </Button>
            </Surface>

        </Surface>
    )

}

const styles = StyleSheet.create({

    background : {
        width : '100%',
        flex : 1,
        backgroundColor : '#ffffff',
    },

    logoContainer : {
        width : '100%',
        flex : 1,
        justifyContent : 'center',
        alignItems : 'center',
        backgroundColor : 'transparent',
    },

    logo : {
        width : '20%',
        resizeMode : 'contain',
    },

    animationContainer : {
        flex : 2,
        width : '100%',
        justifyContent : 'center',
        alignItems : 'center',
        backgroundColor : 'transparent',
    },

    animation : {
        width : '40%',
    },

    titleContainer : 
    {
        flex : 1,
        width : '100%',
        justifyContent : 'center',
        alignItems : 'center',
        backgroundColor : 'transparent',
    },

    title : {
        width : '30%',
        resizeMode : 'contain',
    },

    buttonsContainer : {
        flex : 2,
        justifyContent : 'center',
        alignItems : 'center',
        backgroundColor : 'transparent',
    },

    buttonsLabel : {
        fontFamily : 'Raleway',
        fontSize : 18,
        lineHeight : 21,
    },

    buttonContainer : {
        width : 220,
        borderRadius : 5,
        marginVertical : 15,
    }

})

export default CrossroadScreen ;