import React from 'react' ;
import { createStackNavigator } from '@react-navigation/stack' ;
import { NavigationContainer } from '@react-navigation/native' ;

import SplashScreen from './src/Screens/splashscreen.js' ;
import OnboardingScreen from './src/Screens/onboardingscreen.js' ;
import LoginScreen from './src/Screens/authentication/login.js' ;
import RegisterScreen from './src/Screens/authentication/register.js' ;
import CrossroadScreen from './src/Screens/authentication/crossroad.js' ;

import DummyScreen from './src/Screens/dummyscreen.js' ;
import HomeScreen from './src/Screens/homescreen.js' ;
import AnimationScreen from './src/Screens/animationscreen.js' ;
import MoodboardScreen from './src/Screens/moodboardscreen.js';
import DashboardScreen from './src/Screens/dashboardscreen.js' ;
import SettingsScreen from './src/Screens/settingsscreen.js' ;


import { createStore, combineReducers } from 'redux' ;
import { Provider } from 'react-redux' ;
import firebase from '@react-native-firebase/app' ;
import '@react-native-firebase/database' ;
import '@react-native-firebase/auth' ;
import '@react-native-firebase/firestore'  ;
import { ReactReduxFirebaseProvider, firebaseReducer } from 'react-redux-firebase' ;
import { createFirestoreInstance, firestoreReducer } from 'redux-firestore' ;

import { FIREBASE_APIKEY, FIREBASE_AUTHDOMAIN, FIREBASE_DATABASEURL,
  FIREBASE_PROJECTID, FIREBASE_STORAGEBUCKET, FIREBASE_MESSAGINGSENDERID, 
  FIREBASE_APPID, FIREBASE_MEASUREMENTID } from '@env' ;


const rrfConfig = {
  userProfile: 'users',
  useFirestoreForProfile: true,
}

const firebaseConfig = {
  apiKey : FIREBASE_APIKEY,
  authDomain : FIREBASE_AUTHDOMAIN,
  databaseURL : FIREBASE_DATABASEURL,
  projectId : FIREBASE_PROJECTID,
  storageBucket : FIREBASE_STORAGEBUCKET,
  messagingSenderId : FIREBASE_MESSAGINGSENDERID,
  appId : FIREBASE_APPID,
  measurementId : FIREBASE_MEASUREMENTID,
} ;

try
{
  firebase.initializeApp( firebaseConfig ) ;
}
catch(e)
{
  console.log('annoying error with app name')
}

firebase.firestore() ;

const rootReducer = combineReducers({
  firebase: firebaseReducer,
  firestore: firestoreReducer
})

const initialState = {}
const store = createStore(rootReducer, initialState)
const rrfProps = {
  firebase,
  config: rrfConfig,
  dispatch: store.dispatch,
  createFirestoreInstance
} ;

const Stack = createStackNavigator() ;

const App = () => {

  return (

    <Provider store = {store}>
      <ReactReduxFirebaseProvider {...rrfProps}>

        <NavigationContainer>
          <Stack.Navigator initialRouteName = "SplashScreen">

            <Stack.Screen name = "SplashScreen" component = { SplashScreen } options = {{ headerShown : false }}></Stack.Screen>
            <Stack.Screen name = "OnboardingScreen" component = { OnboardingScreen } options = {{ headerShown : false }}></Stack.Screen>
            <Stack.Screen name = "HomeScreen" component = { HomeScreen } options = {{ headerShown : false }}></Stack.Screen>
            <Stack.Screen name = "AnimationScreen" component = { AnimationScreen } options = {{ headerShown : false }}></Stack.Screen>
            <Stack.Screen name = "MoodboardScreen" component = { MoodboardScreen } options = {{ headerShown : false }}></Stack.Screen>
            <Stack.Screen name = "SettingsScreen" component = { SettingsScreen } options = {{ headerShown : false }}></Stack.Screen>
            <Stack.Screen name = "LoginScreen" component = { LoginScreen } options = {{ headerShown : false }}></Stack.Screen>
            <Stack.Screen name = "RegisterScreen" component = { RegisterScreen } options = {{ headerShown : false }}></Stack.Screen>
            <Stack.Screen name = "CrossroadScreen" component = { CrossroadScreen } options = {{ headerShown : false }}></Stack.Screen>
            <Stack.Screen name = "DashboardScreen" component = { DashboardScreen } options = {{ headerShown : false }}></Stack.Screen>
            <Stack.Screen name = "DummyScreen" component = { DummyScreen } options = {{ headerShown : false }}></Stack.Screen>

          </Stack.Navigator>   
        </NavigationContainer>

      </ReactReduxFirebaseProvider>
    </Provider>
      
  )

} ;

export default App ;
