import React, { useState, useEffect } from 'react' ;
import { StyleSheet } from 'react-native' ;
import { Surface, Text } from 'react-native-paper' ;
import Toggle from 'react-native-toggle-element' ;

import { DashboardOfTheDay, DashboardOfTheWeek } from '../Components/dashboardsComponent';

import LottieView from 'lottie-react-native' ;

import animationWoman from '../Assets/lotties/yogi.json' ;


const DashboardScreen = ( { navigation }) => {

    const fakeData = [
        { timestamp : 'monday', value : 2.8 },
        { timestamp : 'wednesday', value : 3.4 },
        { timestamp : 'thursday', value : 4 },
        { timestamp : 'friday', value : 2.4 },
        { timestamp : 'saturday', value : 3.8 },
        { timestamp : 'sunday', value : 4.2 },
    ]

    const [ numberOfSessions, setNumberOfSessions ] = useState(3) ;
    const [ preferredMode, setPreferredMode ] = useState("Calme") ;

    const [toggleValue, setToggleValue] = useState(false) ;


    return (

        <Surface style = { styles.container }>
            <Surface style = { styles.header }>
                <Surface style = { styles.headerAnimationContainer }>
                    <LottieView
                        source = { animationWoman }
                        autoPlay
                        loop
                    />
                </Surface>
                
                <Text style = { styles.headerTitle }>Tableau de bord</Text>

                <Toggle
                    value = { toggleValue }
                    onPress = { (newState) => setToggleValue(newState) }
                    containerStyle = { styles.toggleContainer }
                    trackBarStyle = {{
                        height : 30,
                    }}
                    trackBar = {{
                        width : 130,
                        radius : 2,
                        activeBackgroundColor : '#7dbbb9',
                        inActiveBackgroundColor : '#7dbbb9',
                    }}
                    thumbButton = {{
                        width: 60,
                        height: 36,
                        radius: 2,
                        activeBackgroundColor : '##2a5d68',
                        inActiveBackgroundColor : '#2a5d68'
                    }}
                    leftComponent = {<Text style = {{ fontSize : 12, color : 'white' }}>Journée</Text>}
                    rightComponent = {<Text style = {{ fontSize : 12, color : 'white' }}>Semaine</Text>}
                />

            </Surface>
            
           
            {
                toggleValue ? <DashboardOfTheWeek /> : <DashboardOfTheDay />
            }

        </Surface>
    )

}

const styles = StyleSheet.create({

    container : {
        flex : 1,
        backgroundColor : '#E5E5E5',
        justifyContent : 'space-between',
    },

    header : {
        height : '30%',
        width : '100%',             
        backgroundColor : 'transparent',
        marginVertical : 20,
    },

    headerAnimationContainer : {
        width : '30%',
        height : '50%',
        backgroundColor : 'transparent',
        justifyContent : 'flex-start',
    },

    headerTitle : {
        fontFamily : 'Raleway',
        fontSize : 24,
        lineHeight : 28,
        fontWeight : '600',
        color : '#2A5D68',
        marginLeft : '8%',
    },

    toggleContainer : {

        width : '100%',
        height : '25%',
        flexDirection : 'column',
        justifyContent : 'flex-end',
        alignItems : 'center',

    },


})

export default DashboardScreen ;


