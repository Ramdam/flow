import React, { useState, useEffect } from 'react' ;
import { View, StyleSheet, Image } from 'react-native' ;
import { Surface, Button, Text } from 'react-native-paper' ;
import LottieView from 'lottie-react-native' ;

import logo_ullo from '../Assets/sprites/logo_ullo.png' ;
import logo_flow from '../Assets/sprites/logo_flow.png' ;
import dashboard from '../Assets/sprites/dummy_dashboard.png' ;
import animationWoman from '../Assets/lotties/yogi.json' ;
import animationCustomization from '../Assets/lotties/onboarding_customization.json' ;
import animationDashboard from '../Assets/lotties/onboarding_dashboard.json' ;

import Clouds from '../Components/clouds.js' ;

const FirstScreen = () => {

    return (

        <Surface style = { styles.screenContainer }>
            <Clouds />

            <View style = { styles.containerLogo}>
                <Image source = { logo_ullo } style = { styles.logo } />
            </View>

            <View style = { styles.containerAnimation}>
                <LottieView
                    source = { animationWoman }
                    autoPlay
                    loop
                    style = { styles.animation }
                />
            </View>

            <View style = { styles.containerGreetings}>
                <Text style = { styles.greetingsText }>Bienvenue sur </Text>
                <Image source = { logo_flow } style = { styles.title } />
            </View>

            <View style = { styles.containerDescription }>
                <Text style = { styles.descriptionText }>Nous nous réjouissons de vous accompagner dans votre voyage vers la compréhension de soi et le lâcher-prise.</Text>
            </View>

            <View style = {{flex : 0.5}}></View>
        </Surface>
    )
}

const SecondScreen = () => {

    return (
        <Surface style = { styles.screenContainer }>
            <Clouds />

            <View style = { styles.containerLogo}>
                <Image source = { logo_ullo } style = { styles.logo } />
            </View>

            <View style = { styles.containerAnimations}>
                <LottieView
                    source = { animationCustomization }
                    autoPlay
                    loop
                    style = { styles.animationCustomization }
                />
            </View>

            <View style = { styles.containerGreetings}>
                <Text style = { styles.greetingsText }>Personnalisez votre séance</Text>
            </View>

            <View style = { styles.containerDescription }>
                <Text style = { styles.descriptionText }>Sélectionnez le mode et l'animation qui vous conviennent pour adapter la séance à vos objectifs.</Text>
            </View>

            <View style = {{flex : 0.5}}></View>

        </Surface>
    )
}

const ThirdScreen = ( { onPressButton } ) => {

    return (
        <Surface style = { styles.screenContainer }>
            <Clouds />

            <View style = { styles.containerLogo}>
                <Image source = { logo_ullo } style = { styles.logo } />
            </View>

            <View style = { styles.containerDashboard}>
            <LottieView
                    source = { animationDashboard }
                    autoPlay
                    loop
                    style = { styles.animationDashboard }
                />
            </View>

            <View style = { styles.containerGreetings}>
                <Text style = { styles.greetingsText }>Suivez votre activité</Text>
            </View>

            <View style = { styles.containerDescription }>
                <Text style = { styles.descriptionText }>Suivez votre progression sur vos tableaux de bord. Développez ainsi votre compréhension des leviers de votre bien-être.</Text>
            </View>


            <View style = { styles.containerCTA }>
                <Button
                    mode = 'contained'
                    color = '#7DBBB9'
                    labelStyle = {{ color : '#ffffff', fontFamily: 'Raleway', fontWeight: 'bold', fontSize: 18, lineHeight: 21, }}
                    style = {{ width : '50%', boxSizing : 'border-box', borderRadius: 10 }}
                    onPress = { onPressButton }
                >COMMENCER</Button>
            </View>

        </Surface>
    )
}

const styles = StyleSheet.create({

    screenContainer : {
        flex : 1,
        width : '100%',
        backgroundColor : '#ffffff',
    },

    containerLogo : {
        width : '100%',
        flex : 1,
        justifyContent : 'center',
        alignItems : 'center',
    },

    logo : {
        width : '20%',
        resizeMode : 'contain',
    },

    containerAnimation : {
        flex : 2,
        width : '100%',
        justifyContent : 'center',
        alignItems : 'center',
    },

    containerAnimations : {
        flex : 2,
        width : '100%',
        flexDirection : 'row',
        justifyContent : 'center',
        alignItems : 'center',
    },

    animation : {
        width : '80%',
    },

    animationCustomization : {
        width : '90%',
    },

    containerDashboard : {
        flex : 2,
        width : '100%',
        justifyContent : 'center',
        alignItems : 'center',
        padding : 20,
    },

    animationDashboard : {
        width : '90%',
    },

    dashboard : {
        width : '70%',
        resizeMode : 'contain',
    },

    containerGreetings : 
    {
        flex : 0.5,
        width : '100%',
        flexDirection : 'row',
        justifyContent : 'center',
        alignItems : 'center',
    },

    greetingsText : {
        fontFamily: 'Raleway',
        fontStyle: 'normal',
        fontWeight: 'bold',
        fontSize: 24,
        lineHeight: 28,
        color: '#2A5D68',
        marginHorizontal : 5,
    },

    title : {
        width : '30%',
        resizeMode : 'contain',
        marginHorizontal : 5,
    },

    containerDescription : 
    {
        flex : 1,
        left : '10%',
        width : '80%',
        justifyContent : 'flex-start',
        alignItems : 'center',
    },

    descriptionText : {

        textAlign : 'center',
        fontFamily: 'Raleway',
        fontStyle: 'normal',
        fontSize: 16,
        lineHeight: 19,
        color: '#000000',

    },

    containerCTA : {
        flex : 0.8,
        width : '100%',
        justifyContent : 'flex-start',
        alignItems : 'center',
    }

})

export { FirstScreen, SecondScreen, ThirdScreen }