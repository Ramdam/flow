import React, { useState, useEffect } from 'react' ;
import { StyleSheet } from 'react-native' ;
import { Surface, Text } from 'react-native-paper' ;




const DashboardOfTheDay = ( { data } ) => {

    const [ numberOfSessions, setNumberOfSessions ] = useState(0) ;
    const [ preferredMode, setPreferredMode ] = useState('Calm') ;

    const [ stars, setStars ] = useState(0) ;

    return (
        <>
            <Surface style = { styles.moduleStatsContainer }>

                <Text style = { styles.moduleTitle }>Activité</Text>
                <Surface style = { styles.moduleContainer }>

                    <Surface style = { styles.radialProgress }>

                    </Surface>
                    <Surface style = { styles.dataDescriptionContainer }>

                        <Surface style = { styles.dataDescriptionItem }>
                            <Text style = { styles.dataDescriptionItemTitle }>Séances réalisées</Text>
                            <Text style = { styles.dataDescriptionItemContent }>{numberOfSessions}</Text>
                        </Surface>

                        <Surface style = { styles.dataDescriptionItem }>
                            <Text style = { styles.dataDescriptionItemTitle }>Mode favori</Text>
                            <Text style = { styles.dataDescriptionItemContent }>{preferredMode}</Text>
                        </Surface>

                        <Surface style = { styles.dataDescriptionItem }>
                            <Text style = { styles.dataDescriptionItemTitle }>Etoiles du jour</Text>
                            <Text style = { styles.dataDescriptionItemContent }>{stars}</Text>
                        </Surface>

                    </Surface>
                </Surface>
            </Surface>

            <Surface style = { styles.moodboardContainer}>

                <Text style = { styles.moduleTitle }>Humeur</Text>

                <Surface style = { styles.moodboardLineChart}>
                </Surface>                 

            </Surface>
        </>
    )
}

const DashboardOfTheWeek = ( { data } ) => {

    return (
        <>
            <Surface style = { styles.moduleStatsContainer }>
                <Text>Weeks</Text>
            </Surface>

            <Surface style = { styles.moodboardContainer}>

            </Surface>
        </>
    )
}


const styles = StyleSheet.create({

    moduleStatsContainer : {
        height : '45%',
        backgroundColor : 'transparent',
        justifyContent : 'space-between',
    },

    moduleStatsContainer : {
        height : '45%',
        backgroundColor : 'transparent',
        justifyContent : 'space-between',
    },

    moduleTitle : {
        fontFamily : 'Raleway',
        fontSize : 24,
        lineHeight : 28,
        fontWeight : 'bold',
        color : '#2A5D68',
        marginLeft : '8%',
        marginTop : 20,
    },

    moduleContainer : {
        width : '100%',
        height : '80%',
        backgroundColor : 'transparent',
        flexDirection : 'row',
    },

    radialProgress : {

        backgroundColor : 'transparent',
        width : '50%',
        height : '100%',
    },

    dataDescriptionContainer : {

        backgroundColor : 'transparent',
        width : '50%',
        flexDirection : 'column',

    },

    dataDescriptionItem : {
        backgroundColor : 'transparent',
        width : '100%',
        flexDirection : 'column',
        marginVertical : 10,
    },

    dataDescriptionItemTitle : {

        fontFamily : 'Raleway',
        fontSize : 14,
        lineHeight : 16,
        fontWeight : '300',
        color : '#000000',
        
    },
    dataDescriptionItemContent : {

        fontFamily : 'Raleway',
        fontSize : 21,
        lineHeight : 24,
        fontWeight : '500',
        color : '#000000',

    },

    moodboardContainer : {
        height : '35%',
        backgroundColor : '#ffffff',
        borderTopLeftRadius : 20,
        borderTopRightRadius : 20,
    },
    moodboardLineChart : { },

})


export { DashboardOfTheDay, DashboardOfTheWeek }

